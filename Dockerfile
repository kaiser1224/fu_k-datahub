# Pull base image.
FROM python:3.7
LABEL maintainer="MDFK"


WORKDIR /app
RUN apt update
RUN apt install build-essential
RUN apt-get install libsasl2-dev
RUN pip install --upgrade pip wheel setuptools
RUN pip uninstall datahub acryl-datahub || true
RUN pip install --upgrade acryl-datahub
RUN echo $(datahub version)
####datahub docker quickstart
RUN pip install 'acryl-datahub[hive]'

ENV TERM xterm

# 時區
ENV TZ "Asia/Taipei"
##ENTRYPOINT ["python", "run_pretraining_google_fast.py"]
ENTRYPOINT [ "/bin/bash" ]